# UM-Pilot

UM bestaat uit twee software componenten.
1. UM
2. Externe vertaalapplicatie

Beiden componenten zijn te vinden in dit project.
In de main is de documentatie voor UM en de externe vertaalapplicatie te vinden.

Deze software is nog in ontwikkeling en is bedoeld als beproeving van de architectuurprincipes en omvat de MVP functionaliteit van UM.
Deze functionaliteit is voldoende om de VUM pilots te kunnen ondersteunen.

De code is beschikbaargesteld onder de EUPL 1.2 voorwaarden
https://www.eupl.eu/1.2/nl/

# Documentatie

* [Documentatie repo](https://gitlab.com/vng-realisatie/um-pilot/documentatie)

## Docker Compose
Hier volgen enkele nadere details over het gebruik van de docker compose file aanwezig in deze git repository.
Zie ook [opzetten UM in Docker](https://gitlab.com/vng-realisatie/um-pilot/documentatie/-/blob/main/DevOps/opzetten%20UM%20in%20Docker.md).

### Vereisten

1. Clone de gateway repository naar een directory naast de directory waarin deze repository gekloond is. Dan zijn de juiste configuratibestanden beschikbaar voor de volume mount naar de gateway docker container.
```
# Vanuit ./um-compose
cd ..
git clone https://gitlab.com/vng-realisatie/um-pilot/keycloak
```
2. Clone de keycloak repository naar een directory naast de directory waarin deze repository gekloond is. Dan zijn de juiste configuratibestanden beschikbaar voor de volume mount naar de keycloak docker container.
```
# Vanuit ./um-compose
cd ..
git clone https://gitlab.com/vng-realisatie/um-pilot/keycloak
```

### Starten van een specifieke versie van de UM applicatie in Docker
1. Bepaal de versie die je wilt opstarten, bijvoorbeeld "UM1.0" of "develop" en gebruik deze voor de environment variabele UM_VERSIONTAG: voer hiertoe het volgende commando uit in een linux shell:
```
export UM_VERSIONTAG=UM1.0
```
2. Start de containers op
```
docker compose up
```

Ad 1. 
Het versienummer wordt gebruikt om de docker images te gebruiken die op de docker hub van VNG staan en zijn getagged met deze versie.
Indien er geen waarde voor UM_VERSIONTAG wordt gezet, dan wordt de default "develop" gebruikt, zoals gedefinieerd in de docker compose file.

### Probleemoplossing

### Gateway of keycloak container crasht bij het opstarten

Zorg dat aan alle vereisten voldaan is, zoals hierboven geschreven. Verwijder daarna de gecrashte containers en start de docker containers opnieuw.

```
docker compose rm keycloak
docker compose rm gateway
docker compose up
```
