
---
# web-applicatie

**details**
image_name:     vngrci/web-applicatie:latest
container_name: web-applicatie

external_ports:
+ 8080 --> 8080
internal_ports:

**build command**
```
ng build --configuration=compose
docker build -f Dockerfile -t vngrci/web-applicatie:TEST .
docker push vngrci/web-applicatie:TEST
```

---
# gateway

**details**
image_name:     vngrci/gateway:latest
container_name: gateway

external_ports:
+ 8081 --> 8080
internal_ports:

**build command**
```
docker build -f Dockerfile -t vngrci/gateway:latest .
docker push vngrci/gateway:latest
```

---
# keycloak

**details**
image_name:     vngrci/keycloak:latest
container_name: keycloak

external_ports:
+ 8083 --> 8080
internal_ports:
+ 8080

**build command**
```
docker build -f resources/docker/Dockerfile -t vngrci/keycloak:latest .
docker push vngrci/keycloak:latest
```

---
# kibana

**details**
image_name:     vngrci/kibana:latest
container_name: kibana

external_ports:
+ 8082 --> 5601
internal_ports:
+ 5601

**build command**
```
docker build -f resources/docker/Dockerfile -t vngrci/kibana:latest .
docker push vngrci/kibana:latest
```

---
# elasticsearch

**details**
image_name:     vngrci/elasticsearch:latest
container_name: elasticsearch

external_ports:
+ 8084 --> 9200
internal_ports:
+ 9200
+ 9300

**build command**
```
docker build -f resources/docker/Dockerfile -t vngrci/elasticsearch:latest .
docker push vngrci/elasticsearch:latest
```

---
# adapter

**details**
image_name:     vngrci/adapter:latest
container_name: adapter

external_ports:
internal_ports:
+ 8080

**build command**
```
mvn dependency:resolve
mvn clean package
docker build -f Dockerfile -t vngrci/adapter:latest .
docker push vngrci/adapter:latest
```

---
# vacatures-bemiddelaar

**details**
image_name:     vngrci/vacatures-bemiddelaar:latest
container_name: vacatures-bemiddelaar

external_ports:
internal_ports:
+ 8080

**build command**
```
mvn dependency:resolve
mvn clean package -DskipTests
docker build -f Dockerfile -t vngrci/vacatures-bemiddelaar:latest .
docker push vngrci/vacatures-bemiddelaar:latest
```

---
# vacatures-bron

**details**
image_name:     vngrci/vacatures-bron:latest
container_name: vacatures-bron

external_ports:
internal_ports:
+ 8080

**build command**
```
mvn dependency:resolve
mvn clean package -DskipTests
docker build -f Dockerfile -t vngrci/vacatures-bron:latest .
docker push vngrci/vacatures-bron:latest
```

---
# werkzoekende-bemiddelaar

**details**
image_name:     vngrci/werkzoekende-bemiddelaar:latest
container_name: werkzoekende-bemiddelaar

external_ports:
internal_ports:
+ 8080

**build command**
```
mvn dependency:resolve
mvn clean package -DskipTests
docker build -f Dockerfile -t vngrci/werkzoekende-bemiddelaar:latest .
docker push vngrci/werkzoekende-bemiddelaar:latest
```

---
# werkzoekende-bron

**details**
image_name:     vngrci/werkzoekende-bron:latest
container_name: werkzoekende-bron

external_ports:
internal_ports:
+ 8080

**build command**
```
mvn dependency:resolve
mvn clean package -DskipTests
docker build -f Dockerfile -t vngrci/werkzoekende-bron:latest .
docker push vngrci/werkzoekende-bron:latest
```
