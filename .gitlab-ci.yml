stages: # List of stages for jobs, and their order of execution 
  - prepare
  - package
  - verify_release
  - finalize_release

variables:
  DOCKER_TLS_CERTDIR: "/certs"
  UM_VERSIONBRANCH: "develop" # The branch from where to create releases
  GITGROUP: vng-realisatie%2Fum-pilot%2F # %2F is encoded /
  UM_VERSIONTAG: $umversie
  
workflow: 
    rules:
     - if: $umversie =~ /UM.*/
     - when: never

###############################################
###    prepare: verifyUmversieIsNotInUse    ###
###############################################

# Checks if um-compose project has no tag with umversie yet.

verifyUmversieIsNotInUse:
  stage: prepare
  variables:
   PROJECT: um-compose 
  script: 
   - echo $UM_VERSIONTAG
   - |- 
      if (curl "https://gitlab.com/api/v4/projects/${GITGROUP}${PROJECT}/repository/tags/${UM_VERSIONTAG}" | grep $UM_VERSIONTAG)
      then 
        echo Unexpected, found $UM_VERSIONTAG for $PROJECT
        exit 400
      else
        echo As expected, not found git repo $PROJECT with tag $UM_VERSIONTAG.
      fi    
  environment:
    name: release
    url: https://example.com
  only:
    variables:
     - $umversie =~ /UM.*/

#######################################
###    prepare: verify .env file    ###
#######################################


# Checks if .env file contains umversie.

verifyUmversieIsInEnv:
  stage: prepare
  script: 
   - echo $UM_VERSIONTAG
   - cat .env | grep VERSION=
   - |-
     if (cat .env | grep VERSION= | grep $UM_VERSIONTAG)
     then
       echo Found $UM_VERSIONTAG in the .env file
     else
       echo Not found VERSION set to $UM_VERSIONTAG in .env file. Please update.
       exit 404
     fi
  environment:
    name: release
    url: https://example.com
  only:
    variables:
     - $umversie =~ /UM.*/
     
###########################################################
###    package: createGitTags_and_pushNewDockerimage    ###
###########################################################

# Pulls image from registry with tag $UM_VERSIONBRANCH and push the copy with umversie tag
# Reads the git hash from the image file and tags the code belonging to the hash with umversie

# keycloak
# kibana
# elasticsearch
createGitTags_and_pushNewDockerimage:
  stage: package
  parallel:
    matrix:
     - PROJECT: [gateway,web-applicatie,camel-adapter,vacatures-bron,vacatures-bemiddelaar,werkzoekendenprofielen-bemiddelaar,werkzoekendenprofielen-bron]
  image: docker:20.10.16
  services:
   - docker:20.10.16-dind
  script: 
   - apk add curl jq
   - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
   - docker pull vngrci/$PROJECT:$UM_VERSIONBRANCH
   - gitrevision=$(docker inspect vngrci/$PROJECT:$UM_VERSIONBRANCH | jq -r '.[0].Config.Labels["org.opencontainers.image.revision"]')
   - echo ${gitrevision}
   - |-
     curl --request POST --header "PRIVATE-TOKEN: ${UM_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${GITGROUP}${PROJECT}/repository/tags?tag_name=${UM_VERSIONTAG}&ref=${gitrevision}"
   - docker tag vngrci/$PROJECT:$UM_VERSIONBRANCH vngrci/$PROJECT:$UM_VERSIONTAG
   - docker push vngrci/$PROJECT:$UM_VERSIONTAG
  needs:
  -  verifyUmversieIsNotInUse
  -  verifyUmversieIsInEnv
  only:
    variables:
     - $umversie =~ /UM.*/
  
############################################
###    verify_release: verify-gitrepo    ###
############################################

# The previous step should have tagged all repos with um versie, let's check.

# keycloak
# kibana
# elasticsearch
verify-gitrepos:
  stage: verify_release
  parallel:
    matrix:
     - PROJECT: [gateway,web-applicatie,camel-adapter,vacatures-bron,vacatures-bemiddelaar,werkzoekendenprofielen-bemiddelaar,werkzoekendenprofielen-bron]
  needs:
  -  createGitTags_and_pushNewDockerimage
  script:
   - echo $GITGROUP, $PROJECT,$UM_VERSIONTAG, $UM_VERSIONTAG 
   - |- 
     if (curl "https://gitlab.com/api/v4/projects/${GITGROUP}${PROJECT}/repository/tags/${UM_VERSIONTAG}" | grep $UM_VERSIONTAG)
     then 
       echo Found $UM_VERSIONTAG for ${PROJECT}
     else
       echo Not found git repo ${PROJECT} with tag $UM_VERSIONTAG.
       exit 404
     fi    

########################################################
###    verify_release: verify-docker-compose-job     ###
########################################################

# Let's check if the docker compose file can be parsed and whether the images are available

verify-docker-compose-job:
  stage: verify_release
  needs:
   -  createGitTags_and_pushNewDockerimage
  image: docker:20.10.16
  services:
   - docker:20.10.16-dind
  script:
   - export UM_VERSIONTAG=$UM_VERSIONTAG
   - |-
     if (docker compose convert | grep $UM_VERSIONTAG)
     then
       echo Found $UM_VERSIONTAG in the docker compose file
     else
       echo Not found $UM_VERSIONTAG in docker compose file. Maybe the setting the $UM_VERSIONTAG did not go well?
       exit 404
     fi
   - docker compose create

######################################################
###    finalize_release: tagUMComposeWithUMversie  ###   
######################################################

# Also tag the um-compose file with um versie.

tagUMComposeWithUMversie:
  stage: finalize_release
  variables:
   PROJECT: um-compose 
  needs:
   - verify-gitrepos
   - createGitTags_and_pushNewDockerimage
  image: registry.gitlab.com/gitlab-org/release-cli
  script: 
   - echo "running release_job for $UM_VERSIONTAG"
  release:                               # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
   tag_name: $UM_VERSIONTAG
   description: 'Released using the CICD pipeline'
   tag_message: 'UM Release: ${UM_VERSIONTAG}'